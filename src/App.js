import React, { Component } from 'react';
import SidebarComponent from './sidebar/sidebar';
import Editor from './editor/editor';
import './App.css';

const firebase = require("firebase");

class App extends Component {
  constructor() {
    super()
    this.state = {
      selectedNoteIndex: null,
      selectedNote: null,
      notes: null
    }
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection('notes')
      .onSnapshot(serverUpdate => {
        const notes = serverUpdate.docs.map(doc => {
          const data = doc.data();
          data['id'] = doc.id;
          return data;
        });
        console.log(notes)
        this.setState({ notes: notes });
      });
  }

  render() {
    return (
      <div className="app-container">
        <SidebarComponent
          selectedNoteIndex={this.state.selectedNoteIndex}
          notes={this.state.notes}
        >
        </SidebarComponent>
        <Editor></Editor>
      </div>
    )
  }
}

export default App;
