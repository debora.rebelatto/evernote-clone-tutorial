import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import List from '@material-ui/core/List';
import { Divider, Button } from '@material-ui/core';
import SidebarItemComponent from '../sidebarItem/sidebarItem';

class SidebarComponent extends Component {
  constructor() {
    super()
    this.state = {
      title: null,
      addingNote: false
    }
  }

  newNoteBtnClick = () => {
    this.setState({ addingNote: !this.state.addingNote })
  }

  updateTitle = (event) => {
    const { name, value, type } = event.target
    this.setState({ [name]: value })
  }

  newNote = () => {
    console.log('new');
  }

  selectNote = () => {
    console.log("")
  }

  deleteNote = () => {
    console.log("")
  }

  render() {
    const { notes, classes, selectedNoteIndex } = this.props;

    if (notes) {
      return (
        <div>
          <Button
            onClick={this.newNoteBtnClick}
            className={classes.newNoteBtn}
          >
            {this.state.addingNote ? 'cancel' : 'new note'}
          </Button>
          {
            this.state.addingNote
              ?
              <div>
                <input
                  type='text'
                  name='title'
                  className={classes.newNoteInput}
                  placeholder="Enter title"
                  onKeyUp={this.updateTitle}
                >
                </input>
                <Button
                  className={classes.newNoteInput}
                  onClick={this.newNote}
                > Add Note
                </Button>
              </div>
              :
              null
          }
          <List>
            {
              notes.map((_note, _index) => {
                return (
                  <div key={_index}>
                    <SidebarItemComponent>
                      _notes={_note}
                      _index={_index}
                      selectedNoteIndex={selectedNoteIndex}
                      selectNote={this.selectNote}
                      deleteNote={this.deleteNote}
                    </SidebarItemComponent>
                    <Divider></Divider>
                  </div>
                )
              })
            }
          </List>
        </div>
      )
    } else {
      return (<div>lkdsjad</div>)
    }
  }
}

export default withStyles(styles)(SidebarComponent);