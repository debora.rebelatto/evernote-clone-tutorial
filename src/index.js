import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

const firebase = require('firebase');
require('firebase/firestore')

// Initialize Firebase
firebase.initializeApp({
  apiKey: "AIzaSyDiOpJgDb2N4d0MJ80u5WAsgRKz9CloxzI",
  authDomain: "evernote-clone-266b6.firebaseapp.com",
  databaseURL: "https://evernote-clone-266b6.firebaseio.com",
  projectId: "evernote-clone-266b6",
  storageBucket: "evernote-clone-266b6.appspot.com",
  messagingSenderId: "641275611030",
  appId: "1:641275611030:web:5dc6b8e623c86a4e60bfb7",
  measurementId: "G-4NTKE1T6V3"
});

firebase.analytics();

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
