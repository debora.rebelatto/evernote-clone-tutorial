import React, { Component } from 'react';
import ReactQuill from 'react-quill';
import debounce from '../helpers';
// import BorderColorIcon from '@material-ui/icons/BorderColor';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

class EditorComponent extends Component {
  constructor() {
    super()
    this.state = {
      text: '',
      title: '',
      id: ''
    }
  }

  updateBody = async(value) => {
    await this.setState({ text: value })
    this.updateBody();
  } 

  updateBody = debounce(() => {
    console.log('update database')
// La8tr
  }, 1500)

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.editorContainer}>
        <ReactQuill
          value={this.state.text}
          onChange={this.updateBody}>

        </ReactQuill>
      </div>
    )
  }
}

export default withStyles(styles)(EditorComponent);
